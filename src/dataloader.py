import numpy as np 
# print(leng)

class ReturnData():
	
	def __init__(self,data):
		self.depths = []
		self.images = []
		self.data = data
		self.leng = self.data.len()
		self.track = 0

	def return_data(self):

		if(self.track == 0):
			for i in range(self.leng):
				image, depth = self.data.getitem(i)
				image = np.array(image)
				self.depths.append(depth['p'])
				self.images.append(image)
				# input()

			self.depths = np.array(self.depths)
			self.images = np.array(self.images)
			self.track+=1
			
		return self.images,self.depths,self.leng

# print(depths[0])