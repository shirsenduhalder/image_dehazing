from PIL import Image
import random
import matplotlib.pyplot as plt 
import numpy as np 
from sklearn.feature_extraction import image

'''
	Returning Hazy patches of an image
'''

def hazy_patches(hazy_image,t_image,patch_h,patch_w,all_patch,A_patches,T_patches):
	
	#returns the illumination A for every image
	# a = return_illum(i)

	#creating hazy patches for every channel using the optical modelling formula
	# hazy_image[i,:,:,0] = (images[i,:,:,0]*t[i])+(1-t[i])*a
	# hazy_image[i,:,:,1] = (images[i,:,:,1]*t[i])+(1-t[i])*a
	# hazy_image[i,:,:,2] = (images[i,:,:,2]*t[i])+(1-t[i])*a

	#gets patches of one image
	patch_one,trans_patch,a_patches = make_patches(hazy_image,t_image,patch_h,patch_w)

	#appends patches of one image
	all_patch.append(patch_one)
	T_patches.append(trans_patch)
	A_patches.append(a_patches)
	# hazy_imageim[i] = hazy_im[i].astype(np.float32)
	# im = Image.fromarray(hazy_im[i])
	# trans_maps = make_trans_patch(i,50,50)
	return (all_patch), (T_patches),(A_patches)

# patches = np.squeeze(np.array(hazy_images(hazy_im,patches,0)))
# del hazy_im
# print(np.squeeze(patches).shape)

'''
	Function for creating hazy patches
'''

def make_patches(image,t_image,patch_h,patch_w):
	h,w,c = image.shape
	patches = []
	t_patches = []
	a_patches = []
	for i in xrange(0,h - patch_h,patch_h/2):
		for j in xrange(0, w - patch_w, patch_w/2):
			hazy_patch = np.zeros((patch_h,patch_w,c))
			a_patches_all = []
			# Illuminance value of patches for single channel
			for i in range(3):
				a_patch = random.uniform(0.6,1.0)*255
				#appending Illuminance values of all channels
				a_patches_all.append(a_patch)
			a_patches.append(a_patches_all)

			t_patch = t_image[i:i + patch_h,j:j+patch_w]
			t_patches.append(t_patch)

			patch = image[i : i+patch_h,j:j+patch_w,:]
			hazy_patch[:,:,0] = (patch[:,:,0]*t_patch)+(1-t_patch)*a_patches_all[0]
			hazy_patch[:,:,1] = (patch[:,:,1]*t_patch)+(1-t_patch)*a_patches_all[1]
			hazy_patch[:,:,2] = (patch[:,:,2]*t_patch)+(1-t_patch)*a_patches_all[2]
			#append single patch
			patches.append(hazy_patch)

	#appends illuminance values of one image to the array
	return patches,t_patches,a_patches

# def make_trans_patch(index,patch_h=50,patch_w=50):
# 	t_patches = []
# 	t_image = t[index]
# 	h,w = t_image.shape
# 	for i in xrange(0,h - patch_h,patch_h/2):
# 		for j in xrange(0, w - patch_w, patch_w/2):
# 			t_patch = t_image[i:i + patch_h,j:j+patch_w]
# 			t_patches.append(t_patch)
# 	T_patches.append(t_patches)
# 	return T_patches