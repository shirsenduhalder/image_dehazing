from keras import objectives
from keras import backend as K

def trloss(y_true,y_pred):
	tr_diff = y_true - y_pred
	tr_loss = K.pow(tr_diff,2)

	return tr_loss

def illoss(y_true,y_pred):
	il_diff = y_true - y_pred
	il_loss	= K.pow(il_diff,2)
	return il_loss

