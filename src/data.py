import numpy as np 
import os 
from PIL import Image
import scipy.io 

EXTENSIONS = ['.jpg']
D_EXTENSIONS = ['.mat']

def load_image(file):
	return Image.open(file)

def is_image(filename):
	return any(filename.endswith(ext) for ext in EXTENSIONS)

def is_depth(filename):
	return any(filename.endswith(ext) for ext in D_EXTENSIONS)

def image_path(root,basename):
	return os.path.join(root,basename)

def image_basename(filename):
	return os.path.basename(os.path.splitext(filename)[0])

class Dataset():

	def __init__(self):
		self.images = os.path.join('../data/nyu_2/','images')
		self.depths = os.path.join('../data/nyu_2/','depths')

		self.image_filenames = sorted([x for x in os.listdir(self.images) if is_image(x)])
		self.depth_filenames = sorted([x for x in os.listdir(self.depths) if is_depth(x)])


	def getitem(self,index):
		# print(self.image_filenames, self.depth_filenames)
		image_files = self.image_filenames[index]
		depth_files = self.depth_filenames[index]

		with open(image_path(self.images,image_files),'rb') as f:
			image = load_image(f).convert('RGB')
			# print(type(image))
			# image.show()
		with open(image_path(self.depths,depth_files),'rb') as f:
			depth = scipy.io.loadmat(f)
			# print(type(depth))
		
		return image,depth

	def len(self):
		return len(self.image_filenames)