import keras
from keras.datasets import cifar10
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential, Model, load_model
from keras.layers import Dense, Dropout, Activation, Flatten, Input, concatenate
from keras.layers import Conv2D, MaxPooling2D
from keras import backend as K
import os
from hazyimage import *
from loss import *
from transmittancemap import *
from data import *
from dataloader import * 
from keras.utils.generic_utils import get_custom_objects

''' 
Model hyperparams

'''
batch_size = 32
epochs = 100
get_custom_objects().update({"trloss": trloss,"illoss":illoss})
save_dir = os.path.join(os.getcwd(),'../models')

'''
CONSTANTS
'''
PATCH_H = 50
PATCH_W = 50
TRANSMISSION_DATA = os.path.join(os.getcwd(),'../data/numpy_data/transmission_map.npy')
PATCHES_DATA = os.path.join(os.getcwd(),'../data/numpy_data/patch_data.npy')
T_PATCH_DATA = os.path.join(os.getcwd(),'../data/numpy_data/T_patch_data.npy')
A_PATCHES_DATA = os.path.join(os.getcwd(),'../data/numpy_data/A_patch_data.npy')

'''
	Loading the transmittance maps from transmittancemap.py
'''

images_data = Dataset()
data = ReturnData(images_data)

images,depths,leng = data.return_data()

if(os.path.isfile(TRANSMISSION_DATA)):
	t = np.load(TRANSMISSION_DATA)
else:
	t = create_transmission_map(images,depths,leng)
	np.save('../data/numpy_data/transmission_map.npy',t)

#to store hazy patches of all images
all_patch = []

#to store the ground truth illuminance of patches
A_patches = []

#to store the ground truth patches of transmittance maps
T_patches = []


'''
Loading patches and transmittance patches
'''
if(os.path.isfile(PATCHES_DATA) and (os.path.isfile(T_PATCH_DATA)) and (os.path.isfile(A_PATCHES_DATA))):
	all_patch = np.load(PATCHES_DATA)
	T_patches = np.load(T_PATCH_DATA)
	A_patches = np.load(A_PATCHES_DATA)
else:
	for i in range(leng):
		if(i%20==0):
			print(i)
		all_patch,T_patches,A_patches = hazy_patches(images[i],t[i],PATCH_H,PATCH_W,all_patch,A_patches,T_patches)
	number_images = len(all_patch)
	number_patches = len(all_patch[0])
	all_patch = np.array(all_patch)
	all_patch = (all_patch/255.0).reshape(number_patches*number_images,PATCH_H,PATCH_H,3)
	T_patches = np.array(T_patches)
	T_patches = T_patches.reshape(number_images*number_patches,PATCH_H * PATCH_W)
	A_patches = np.array(A_patches)/255.0
	A_patches = A_patches.reshape(number_patches*number_images,3)
	np.save(PATCHES_DATA,all_patch)
	np.save(T_PATCH_DATA,T_patches)
	np.save(A_PATCHES_DATA,A_patches)

'''
Loading illuminance values for patches
'''	


x = Input(shape=all_patch.shape[1:])
# print(x)
# print(x.shape)

x1 = Conv2D(8,(5,5),strides=(1,1),activation='tanh')(x)
x2 = Conv2D(8,(5,5),strides=(1,1),activation='tanh')(x)
x3 = Conv2D(8,(5,5),strides=(1,1),activation='tanh')(x)

x1 = Conv2D(8,(15,15),strides=(1,1),activation='tanh')(x1)
x2 = Conv2D(8,(15,15),strides=(1,1),activation='tanh')(x2)
x3 = Conv2D(8,(10,10),strides=(1,1),activation='tanh')(x3)

x1 = Conv2D(16,(10,10),strides=(1,1),activation='tanh')(x1)
x1 = Conv2D(8,(9,9),strides=(1,1),activation='tanh')(x1)

x2 = Conv2D(16,(12,12),strides=(1,1),activation='tanh')(x2)

x3 = Conv2D(8,(7,7),strides=(1,1),activation='tanh')(x3)
x3 = Conv2D(8,(6,6),strides=(1,1),activation='tanh')(x3)
x3 = Conv2D(8,(6,6),strides=(1,1),activation='tanh')(x3)

fusion_1 = concatenate([x2,x3])
fusion_1 = Conv2D(8,(7,7),strides=(1,1),activation='tanh')(fusion_1)

fusion_2 = concatenate([fusion_1,x1])
fusion_2 = Flatten()(fusion_2)

dense = Dense(40,activation='tanh')(fusion_2)
T = Dense(PATCH_H * PATCH_W,activation='sigmoid')(dense)
A = Dense(3,activation='sigmoid')(dense)
# T = Activation('tanh')(dense1)
# A = Activation('tanh')(dense2)

model = Model(inputs=[x],outputs=[T,A])
# model.summary()
# model=load_model('../models/1_mod.h5')
model.compile(optimizer='Adagrad',loss=[trloss,illoss])

model.fit(all_patch[:-100000] ,[T_patches[:-100000],A_patches[:-100000]],epochs=200,shuffle=True,verbose=True,batch_size=64)

# inp = model.input                                           # input placeholder
# outputs = [layer.output for layer in model.layers]          # all layer outputs
# functors = [K.function([inp]+ [K.learning_phase()], [out]) for out in outputs]  # evaluation functions

# # Testing
# test = np.random.random(input_shape)[np.newaxis,...]
# layer_outs = [func([test, 1.]) for func in functors]
# print layer_outs
'''
	Function to show images side by side in matplotlib
'''
# def show_images(images, cols = 1, titles = None):
#     assert((titles is None)or (len(images) == len(titles)))
#     n_images = len(images)
#     if titles is None: titles = ['Image (%d)' % i for i in range(1,n_images + 1)]
#     fig = plt.figure()
#     for n, (image, title) in enumerate(zip(images, titles)):
#         a = fig.add_subplot(cols, np.ceil(n_images/float(cols)), n + 1)
#         if image.ndim == 2:
#             plt.gray()
#         plt.imshow(image)
#         a.set_title(title)
#     fig.set_size_inches(np.array(fig.get_size_inches()) * n_images)
# plt.show()