import numpy as np
from loss import *
from keras.models import Sequential, Model,load_model
from PIL import Image
from keras.utils.generic_utils import get_custom_objects
import matplotlib.pyplot as plt 
get_custom_objects().update({"trloss": trloss,"illoss":illoss})
t = load_model('../models/2_mod.h5')

image = Image.open('../data/rand.jpg')
image = np.array(image)/255.0

def dehaze(model,image,patch_h=50,patch_w=50):
	h,w,c = image.shape
	T_arr=np.zeros((h,w))
	Count=np.zeros((h,w))
	A_arr=np.zeros((h,w,c))
	a_patches = []
	for i in xrange(0,h - patch_h,patch_h/2):
		for j in xrange(0, w - patch_w, patch_w/2):

			# Illumimance value for patches for all channels
			
			patch = image[i : i+patch_h,j:j+patch_w,:]

			#appends values of illuminance of a single patch
			a_patches.append(patch)

			#append single patch

	#appends illuminance values of one image to the array
	np_patches = np.array(a_patches)
	T, A = model.predict(np_patches)
	T = T.reshape((-1,50,50))
	c=0
	for i in xrange(0,h - patch_h,patch_h/2):
		for j in xrange(0, w - patch_w, patch_w/2):

			T_arr[i : i+patch_h,j:j+patch_w]=T_arr[i : i+patch_h,j:j+patch_w]+T[c]
			A_arr[i : i+patch_h,j:j+patch_w,:]=A_arr[i : i+patch_h,j:j+patch_w,:]+A[c]

			Count[i : i+patch_h,j:j+patch_w]=Count[i : i+patch_h,j:j+patch_w]+1
			c=c+1
	return 	T,T_arr,A_arr,Count

T,t, A, count = dehaze(t,image)

t[count!=0] = t[count!=0]/count[count!=0]
count=count[:,:,np.newaxis]
count = np.tile(count,(1,1,3))
A = A/count

t=t[:,:,np.newaxis]
t = np.tile(t,(1,1,3))

J=(image-(1-t)*A)/t
J = J.clip(0,1)
J[np.isnan(J)]=0
plt.figure()
plt.imshow(J)
plt.show()
