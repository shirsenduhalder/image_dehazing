import scipy.io as sio
import numpy as np
#from __future__ import print_function
import keras
#from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten,Input
from keras.layers import Conv2D,Conv3D, MaxPooling2D ,MaxPooling3D
from keras import backend as K
from keras import optimizers , metrics
from keras.models import Model,load_model
from keras.layers.core import Activation,Reshape
from keras.utils.generic_utils import get_custom_objects
from init import *
import matplotlib.pyplot as plt


#PATCH_X=100
#PATCH_Y=100
#PATCH_Z=10



NUM_CHANNEL=3
batch_size = 10
epochs=10

img_rows=400
img_cols=300

#get_custom_objects().update({'metric_A':metric_A})

input_shape = (img_rows, img_cols, NUM_CHANNEL)   #img_rows=400, img_cols=300

rate=0.5  #dropout rate

#model = load_model(A_NEW_MODEL)

x=Input(shape=(input_shape))
'''
x1=Conv2D(16,(11,11),strides=(3, 3),padding='valid', activathttps://github.com/shirsenduhalder/deepcolour/blob/master/net/model.ipynbion="elu", bias_initializer='zeros')(x)
x1=Conv2D(8,(7,7),strides=(3, 3), activation="elu", bias_initializer='zeros')(x1)
x1=MaxPooling2D(pool_size=(2, 2),strides=2)(x1)
x1=Conv2D(4,(5,5),  activation="elu" )(x1)
x1=Conv2D(4,(5,5),  activation="elu" )(x1)
x1=Conv2D(4,(3,3), activation="elu")(x1)
x1=Conv2D(4,(3,3), activation="elu")(x1)
x1=Conv2D(2,(3,3),activation="elu",)(x1)
x1=MaxPooling2D(pool_size=(2, 2))(x1)
'''

x1=Conv2D(8,(7,7),strides=(3,3), activation="elu", bias_initializer='zeros')(x)
x1=Conv2D(8,(5,5),  activation="elu" )(x1)
x1=Conv2D(8,(5,5),  activation="elu" )(x1)
x1=MaxPooling2D(pool_size=(2, 2),strides=2)(x1)
x1=Conv2D(4,(3,3), activation="elu")(x1)
x1=Conv2D(4,(3,3), activation="elu")(x1)
x1=Conv2D(4,(3,3), activation="elu")(x1)
x1=Conv2D(2,(3,3), activation="elu")(x1)
x1=MaxPooling2D(pool_size=(2, 2))(x1)

#fully connected layer section
x1=Flatten()(x1)
x1=Dropout(rate, noise_shape=None, seed=None)(x1) #dropout layer
x1=Dense(20)(x1)
x1=Activation("elu")(x1)
x1=Dropout(rate, noise_shape=None, seed=None)(x1) #dropout layer
x1=Dense(10)(x1)
x1=Activation("elu")(x1)
x1=Dense(3)(x1)
x1=Activation("sigmoid")(x1)


model=Model(inputs=x,outputs=x1)

model.summary()

model.compile(loss='loss_A',optimizer="adadelta",metrics=['cos_A','mae'])
#test_t=B['test_t']
#test_t=B['test_t']
D=np.load(NUMPY_DATA_A)
X=D['X']
Y=D['Y']

history = model.fit(X, Y,  epochs=50, batch_size=10, verbose=1)
# list all data in history
'''
print history.history.keys() 
plt.plot(history.history['loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()
'''
#model.fit(X,Y,batch_size=batch_size,epochs=epochs,verbose=1)

model.save(A_MODEL)


