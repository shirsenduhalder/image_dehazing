import scipy.io as sio
import numpy as np
import keras
from keras.models import Sequential, load_model
from keras.layers import Dense, Dropout, Flatten , Input
from keras.layers import Conv2D, Conv3D, MaxPooling2D
from keras import backend as K
from keras.models import Model
from keras import optimizers , metrics
from keras.layers.core import Activation,Reshape
from keras.utils.generic_utils import get_custom_objects
from init import *
from keras.layers.merge import Concatenate
from keras.layers.convolutional import  UpSampling2D
from keras import regularizers
import matplotlib.pyplot as plt
from keras.layers.wrappers import TimeDistributed





NUMPY_DATA_PATCH="/home/ecsuiplab/Dehaze/data/numpy_data/data_patch"
NUMPY_DATA="/home/ecsuiplab/Dehaze/data/numpy_data/data_all"




img_rows, img_cols =480, 640


input_shape1 = (img_rows, img_cols, 3)
input_shape2 = (img_rows, img_cols,1)

in1_x=Input(shape=(input_shape1))
in2_x=Input(shape=(input_shape2))



x2=Conv2D(1,(3,3),activation="tanh")(in2_x)
#for A
x1=Conv2D(3,(3,3),activation="tanh")(in1_x)

x3=Concatenate()([x1,x2])
x3=Conv2D(5,(3,4),strides=(2,2),activation="tanh")(x3)
x3=Conv2D(5,(3,4),strides=(2,2),activation="tanh")(x3)
x3=Conv2D(5,(3,4),strides=(2,2),activation="tanh")(x3)
x3=Conv2D(5,(3,4),strides=(2,2),activation="tanh")(x3)
x3=Conv2D(5,(3,4),strides=(2,2),activation="tanh")(x3)
x3=Conv2D(5,(3,4),activation="tanh")(x3)
x3=Conv2D(5,(3,4),activation="tanh")(x3)
x3=Flatten()(x3)
x3=Dense(3)(x3)
x3=Activation("sigmoid")(x3)



model=Model(inputs=[in1_x,in2_x],outputs=[x3])



model.summary()
model.compile(loss='mae',optimizer="adagrad",metrics=[metrics.mae])

#test_t=B['test_t']
#test_t=B['test_t']
D=np.load(NUMPY_DATA+".npz")
X1=D['HI']
X2=D['T']
Y=D['A']

X2=X2[:,:,:,np.newaxis] 
Y=Y[:,0,:]
model=load_model(AT_MODEL)
model.fit([X1[:-200],X2[:-200]],Y[:-200],batch_size=10,epochs=9999999,verbose=1,
	validation_data=([X1[-200:],X2[-200:]],Y[-200:]))
#model.fit(X[0:1200000],Y[0:1200000],batch_size=batch_size,epochs=epochs,verbose=1,validation_data=(X[1200000:],Y[1200000:]))

#model.save(AT_MODEL)
