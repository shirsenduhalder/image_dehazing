from math import exp
import random
import numpy as np 
import matplotlib.pyplot  as plt
# print(images.shape,depths.shape)
def create_transmission_map(images,depths,leng):

	transmission_map = []
	for i in range(leng):
		image = images[i]
		depth = depths[i]
		h,w,c = image.shape
		single_transmission = np.zeros((h,w))
		beta = random.uniform(0.4,1)
		t = np.exp(depth*(-beta))
		# i = np.array(image*t + A*(1-t))
		transmission_map.append(t)

	return np.array(transmission_map)